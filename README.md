#FR
Voici un petit jeu que j'ai nommé Ball-Rolling.
Pour lancer le jeu il suffit simplement de cloner le projet ou de le télécharger en .zip.
Puis aller dans le dossier "Builds" et lancer le fichier "BallRolling.exe".

#ENG
Here's a little game I called Ball-Rolling.
To launch the game you just have to clone the project or download it in .zip format.
Then go to the "Builds" folder and run the "BallRolling.exe" file.