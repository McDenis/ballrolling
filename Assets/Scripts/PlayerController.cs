﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class PlayerController : MonoBehaviour
{

    // Ball speed
    public float speed;

    // Ball Rigidbody
    public Rigidbody rb;

    // Count points
    public int count;

    // Display number of points
    public Text pointsText;
    public Text collectText;

    // Create timer
    public float timer = 2;

    // Number of jump
    public int NBjump = 0;
       
    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        pointsText.text = "";
        collectText.text = "Collect Golden Cubes";
        
        // Destroy the text after a time
        Destroy(collectText, timer);

        // Detect if is on the ground
        //onGround = true;
    }

    // FixedUpdate
    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);

        // The ball can jump
        if (Input.GetKeyDown(KeyCode.Space) && NBjump < 2)
        {
            GetComponent<Rigidbody>().AddForce(Vector3.up * 400f);
            NBjump++;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {             
            other.gameObject.SetActive(false);
            count++;
            pointsText.text = "Points: " + count.ToString();
            SetCountText();
        }

    }

    void SetCountText()
    {
        if (count >= 20)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
            Cursor.visible = true;
        }

    }

    void OnCollisionEnter(Collision collisionInfo)
    {
        if (collisionInfo.collider.CompareTag("Floor"))
        {
            NBjump = 0;
        }
    }

}


